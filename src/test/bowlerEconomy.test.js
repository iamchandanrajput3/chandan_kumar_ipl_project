const bowlerEconomy = require('../server/bowlerEconomy.js');
const matches = require('../test/testData/matchesTestData.js');
const deliveries = require('../test/testData/deliveriesTestData.js');

test('throw error if not array', () =>{
    expect(() =>{ bowlerEconomy('hello',10); }).toThrow();
});

test('throw error if empty array', () =>{
    expect(()=>{ bowlerEconomy([],[]); }).toThrow();
});

test('throw error if undefined', () =>{
    expect(()=>{ bowlerEconomy(); }).toThrow();
});

test('testing bowlerEconomy function', () =>{
    expect(bowlerEconomy(matches,deliveries)).toEqual(
        [
            ['Z Khan', {
                'totalRuns': 10,
                'balls': 6,
                'economy': '10.00'
            }]
        ]
    );
});
