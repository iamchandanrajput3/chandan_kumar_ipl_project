const extraRunsPerTeam = require('../server/extraRunsPerTeam.js');
const matches = require('../test/testData/matchesTestData.js');
const deliveries = require('../test/testData/deliveriesTestData.js');

test('throw error if not array', () =>{
    expect(() =>{ extraRunsPerTeam('hello',10); }).toThrow();
});

test('throw error if empty array', () =>{
    expect(()=>{ extraRunsPerTeam([],[]); }).toThrow();
});

test('throw error if undefined', () =>{
    expect(()=>{ extraRunsPerTeam(); }).toThrow();
});

test('testing extraRunsPerTeam function', ()=>{
    expect(extraRunsPerTeam(matches,deliveries)).toEqual(
        {
            'Royal Challengers Bangalore': 6
        });
});
