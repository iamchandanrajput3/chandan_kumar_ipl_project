const matchesPerYear = require('../server/matchesPerYear.js');
const matches = require('../test/testData/matchesTestData.js');

test('throw error if not array', () =>{
    expect(() =>{ matchesPerYear(10); }).toThrow();
});

test('throw error if empty array', () =>{
    expect(()=>{ matchesPerYear([]); }).toThrow();
});

test('throw error if undefined', () =>{
    expect(()=>{ matchesPerYear(); }).toThrow();
});

test('give matches played per season', () =>{
    expect(matchesPerYear(matches)).toEqual({'2016': 3, '2017': 1, '2015': 2});
});




