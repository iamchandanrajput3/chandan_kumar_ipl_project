const matchesWonPerYear = require('../server/matchesWonPerYear.js');
const matches = require('../test/testData/matchesTestData.js');

test('throw error if not array', () =>{
    expect(() =>{ matchesWonPerYear('hello'); }).toThrow();
});

test('throw error if empty array', () =>{
    expect(()=>{ matchesWonPerYear([]); }).toThrow();
});

test('throw error if undefined', () =>{
    expect(()=>{ matchesWonPerYear(); }).toThrow();
});

test('testing matchesWonPerYear function', ()=>{
    expect(matchesWonPerYear(matches)).toEqual(
        {
            '2017': {'Rising Pune Supergiant': 1},
            '2016': {'Sunrisers Hyderabad': 1,'Kolkata Knight Riders': 2},
            '2015': {'Kings XI Punjab': 1, 'Royal Challengers Bangalore': 1}
        });
});