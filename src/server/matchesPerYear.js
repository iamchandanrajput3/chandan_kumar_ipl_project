const matchesPerYear = (matchesData) => {

    if(!Array.isArray(matchesData) || typeof matchesData === 'undefined' || matchesData.length === 0)
    {
        throw new Error("Invalid Argument passed");
    }
    
    let matches = matchesData.reduce((obj,match) =>{
        let season = match.season;
        obj.hasOwnProperty(season) ? ++obj[season] : obj[season] = 1;
        return obj;
    },{});

return matches;
}

module.exports = matchesPerYear;