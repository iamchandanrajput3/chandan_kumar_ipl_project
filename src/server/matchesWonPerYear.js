const matchesWonPerYear = (matchesData) =>{

    if(!Array.isArray(matchesData) || typeof matchesData === 'undefined' || matchesData.length === 0)
    {
        throw new Error("Invalid Argument passed");
    }
    
    var matchesWon = matchesData.reduce((obj,match) =>{
        let {season,winner} = match;
        if(!obj.hasOwnProperty(season))
        {
            obj[season] = {};
        }
        obj[season].hasOwnProperty(winner) ? obj[season][winner] +=1 : obj[season][winner] = 1;
        return obj;
    },{});
        
    return matchesWon;
}

module.exports = matchesWonPerYear;