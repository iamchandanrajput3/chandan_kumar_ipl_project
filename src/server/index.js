const csvtojson = require('csvtojson')
const fs = require('fs')

csvtojson()
.fromFile('../data/matches.csv')
.then((json) => {
    fs.writeFileSync("../../matches.json",JSON.stringify(json,null,4),"utf-8",(err) => {
        if(err) console.log(err)
    })
})
csvtojson()
.fromFile('../data/deliveries.csv')
.then((json) => {
    fs.writeFileSync("../../deliveries.json",JSON.stringify(json,null,4),"utf-8",(err) => {
        if(err) console.log(err)
    })
})

const matchesData = require('../../matches.json');
const deliveriesData = require('../../deliveries.json');

const matchesPerYear = require('./matchesPerYear.js');
const matchesWonPerYear = require('./matchesWonPerYear.js');
const extraRunsPerTeam =require('./extraRunsPerTeam.js');
const bowlerEconomy = require('./bowlerEconomy.js');

let matches = {};
try{
    matches = matchesPerYear(matchesData);
    fs.writeFile('../public/output/matchesPerYear.json' , JSON.stringify(matches,null,4) , 'utf-8' ,(err) =>{
        if(err) console.log(err);
    });
} catch(e){
    console.log(e);
}
console.log(matches);


let matchesWon = {};
try{
    matchesWon = matchesWonPerYear(matchesData);
    fs.writeFile('../public/output/matchesWonPerYear.json' , JSON.stringify(matchesWon,null,4) , 'utf-8' , function(err){
        if(err) console.log(err);
    });
} catch(e){
    console.log(e);
}
console.log(matchesWon);


let extraRuns = {};
try{
    extraRuns = extraRunsPerTeam(matchesData,deliveriesData);
    fs.writeFile('../public/output/extraRunsPerYear.json' , JSON.stringify(extraRuns,null,4), 'utf-8' , (err) =>{
        if(err) console.log(err);
    });
}catch(e){
    console.log(e);
}
console.log(extraRuns);


let topBowlersEco = {};
try{
    topBowlersEco = bowlerEconomy(matchesData,deliveriesData);
    fs.writeFile('../public/output/bowlerEconomy.json', JSON.stringify(topBowlersEco,null,4), function(err){
        if(err) console.log(err);
    });
} catch(e){
    console.log(e);
}
console.log(topBowlersEco);