const getMatchId = require('./helperFunction.js');

const extraRunsPerTeam = (matchesData, deliveriesData) => {

    if(!Array.isArray(matchesData) || typeof matchesData === 'undefined' || matchesData.length === 0)
    {
        throw new Error("Invalid Argument passed");
    }
    if(!Array.isArray(deliveriesData) || typeof deliveriesData === 'undefined' || deliveriesData.length ===0)
    {
        throw new Error("Invalid Argument passed");
    }

    let matchId = getMatchId(2016,matchesData);
    let extraRunsPerTeam = deliveriesData.reduce((obj,match) =>{
        let {match_id,bowling_team,extra_runs} = match;
        if(matchId.includes(Number(match_id)))
        {
            obj.hasOwnProperty(bowling_team) ? obj[bowling_team] += Number(extra_runs) : obj[bowling_team] = Number(extra_runs);
        }
        return obj;
    },{});
        
    return extraRunsPerTeam;
}

module.exports = extraRunsPerTeam;