const getMatchId = (year,matchesData) => {
    let matchId = [];

    matchesData.forEach((obj) =>{
        if(year === Number(obj.season))
        {
            matchId.push(Number(obj.id));
        }
    })
    
    return matchId;
}

module.exports = getMatchId;