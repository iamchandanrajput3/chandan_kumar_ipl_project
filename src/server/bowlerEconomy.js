const getMatchId = require('./helperFunction.js');

const getRuns = (matchId,deliveriesData) =>{
    let bowlerRuns = deliveriesData.reduce((obj,match) =>{

        let {match_id,bowler,total_runs,wide_runs,noball_runs} = match;
        if(matchId.includes(Number(match_id)))
        {
            if(obj.hasOwnProperty(bowler))
            {
                obj[bowler].totalRuns += Number(total_runs);
                
            }
            else{
                obj[bowler] = {};
                obj[bowler].totalRuns = Number(total_runs); 
            }
            if(Number(wide_runs)=== 0 && Number(noball_runs)===0)
                {
                    obj[bowler].balls = obj[bowler].balls + 1 || 1; 
                }
        }
        return obj;
    },{});
    
    return bowlerRuns;
}

const bowlerEconomy = (matchesData,deliveriesData) => {

    if(!Array.isArray(matchesData) || typeof matchesData === 'undefined' || matchesData.length === 0)
    {
        throw new Error("Invalid Argument passed");
    }
    if(!Array.isArray(deliveriesData) || typeof deliveriesData === 'undefined' || deliveriesData.length === 0)
    {
        throw new Error("Invalid Argument passed");
    }

    let matchId = getMatchId(2015,matchesData);
let bowlers = getRuns(matchId,deliveriesData);
    
    Object.entries(bowlers).forEach((ele) =>{
        ele[1].economy = (ele[1].totalRuns/(ele[1].balls/6)).toFixed(2);
    });

    var bowlersEconomy = Object.entries(bowlers).sort((a,b) => {
        let curr = a[1].economy;
        let next = b[1].economy;
        return curr-next;
    });
    
    topBowlersEco = bowlersEconomy.slice(0,10);

    return topBowlersEco;
}

module.exports = bowlerEconomy;